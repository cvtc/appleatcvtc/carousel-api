import pytest
import requests
import responses
from requests.auth import AuthBase

from carousel_api.carousel import Carousel

MOCK_AUTH_STRING = "This is a MockAuth"
EXPECTED_AUTH = {"Authorization": MOCK_AUTH_STRING}
EXAMPLE_CAROUSEL_URL = "https://example.carouselsignage.net"
EXPECTED_JSON = {"test": "test_get_request"}
EXPECTED_LIST = [{"test": "test_get_request"}]


class CarouselTest(Carousel):
    def __init__(self, base_url: str, auth: str):
        self.base_url = base_url + "/CarouselAPI"
        self.session = requests.Session()
        self.auth = auth


class MockAuth(AuthBase):
    def __call__(self, r):
        r.headers["Authorization"] = MOCK_AUTH_STRING
        return r


@pytest.fixture
def carousel():
    return CarouselTest(EXAMPLE_CAROUSEL_URL, MockAuth())


def carousel_url(endpoint):
    """
    Adds an endpoint to the base Carousel URL

    :param endpoint:
        API endpoint section of the URL

    :returns: Full endpoint url
    """
    return EXAMPLE_CAROUSEL_URL + "/CarouselAPI" + endpoint


"""
Bulletins
"""


@responses.activate
def test_get_bulletins(carousel):
    """
    Ensures that get_bulletins returns JSON when used without optional params
    """
    responses.add("GET", carousel_url("/v1/Bulletins"), json=EXPECTED_JSON, status=200)
    assert carousel.get_bulletins(1001) == EXPECTED_JSON


@responses.activate
def test_get_bulletins_optional_params(carousel):
    """
    Ensures that get_bulletins returns JSON when used with all optional params
    """
    responses.add("GET", carousel_url("/v1/Bulletins"), json=EXPECTED_JSON, status=200)
    assert carousel.get_bulletins(1001, "2023-08-08T09:24:29.98") == EXPECTED_JSON


@responses.activate
def test_get_bulletin(carousel):
    """
    Ensures that get_bulletin returns JSON when used with required params
    """
    responses.add(
        "GET",
        carousel_url("/v1/Bulletins/asd123asd123"),
        json=EXPECTED_JSON,
        status=200,
    )
    assert carousel.get_bulletin("asd123asd123") == EXPECTED_JSON


@responses.activate
def test_get_bulletin_404(carousel):
    """
    Ensures that get_bulletin raises a HTTPError when returning a 404 response
    """
    responses.add("GET", carousel_url("/v1/Bulletins/asd123asd123"), status=404)
    with pytest.raises(requests.HTTPError):
        carousel.get_bulletin("asd123asd123")


@responses.activate
def test_update_bulletin(carousel):
    """
    Ensures that update_bulletin returns JSON when used with required params
    """
    responses.add(
        "PUT",
        carousel_url("/v1/Bulletins/asd123asd123"),
        json=EXPECTED_JSON,
        status=201,
    )
    assert carousel.update_bulletin("asd123asd123", EXPECTED_JSON) == EXPECTED_JSON


@responses.activate
def test_delete_bulletins(carousel):
    """
    Ensures that delete_bulletins returns a success message when used without
    optional params
    """
    responses.add(
        "DELETE", carousel_url("/v1/Bulletins"), json=EXPECTED_JSON, status=200
    )
    assert (
        carousel.delete_bulletins(["asd123asd123"])
        == "Bulletins successfully queued for deletion."
    )


@responses.activate
def test_delete_bulletins_optional_params(carousel):
    """
    Ensures that delete_bulletins returns a success message when used with all
    optional params
    """
    responses.add(
        "DELETE", carousel_url("/v1/Bulletins"), json=EXPECTED_JSON, status=200
    )
    assert (
        carousel.delete_bulletins(["asd123asd123"], delete_linked=True)
        == "Bulletins successfully queued for deletion."
    )


"""
Channels
"""


@responses.activate
def test_get_channels(carousel):
    """
    Ensures that get_channels returns JSON when used
    """
    responses.add("GET", carousel_url("/v1/Channels"), json=EXPECTED_JSON, status=200)
    assert carousel.get_channels() == EXPECTED_JSON


@responses.activate
def test_get_channel(carousel):
    """
    Ensures that get_channel returns JSON when used with required params
    """
    responses.add(
        "GET", carousel_url("/v1/Channels/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.get_channel(1001) == EXPECTED_JSON


@responses.activate
def test_create_channel(carousel):
    """
    Ensures that create_channel returns JSON when used with required params
    """
    responses.add("POST", carousel_url("/v1/Channels"), json=EXPECTED_JSON, status=200)
    assert carousel.create_channel(EXPECTED_JSON) == EXPECTED_JSON


@responses.activate
def test_update_channel(carousel):
    """
    Ensures that update_channel returns JSON when used with required params
    """
    responses.add(
        "PUT", carousel_url("/v1/Channels/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.update_channel(1001, EXPECTED_JSON) == EXPECTED_JSON


@responses.activate
def test_get_channel_blocks(carousel):
    """
    Ensures that get_channel_blocks returns JSON when used
    """
    responses.add(
        "GET", carousel_url("/v1/ChannelBlocks"), json=EXPECTED_JSON, status=200
    )
    assert carousel.get_channel_blocks() == EXPECTED_JSON


@responses.activate
def test_get_channel_block(carousel):
    """
    Ensures that get_channel_block returns JSON when used with required params
    """
    responses.add(
        "GET", carousel_url("/v1/ChannelBlocks/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.get_channel_block(1001) == EXPECTED_JSON


@responses.activate
def test_create_channel_block(carousel):
    """
    Ensures that create_channel_block returns JSON when used with required
    params
    """
    responses.add(
        "POST", carousel_url("/v1/ChannelBlocks"), json=EXPECTED_JSON, status=200
    )
    assert carousel.create_channel_block(EXPECTED_JSON) == EXPECTED_JSON


@responses.activate
def test_update_channel_block(carousel):
    """
    Ensures that update_channel_block returns JSON when used with required
    params
    """
    responses.add(
        "PUT", carousel_url("/v1/ChannelBlocks/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.update_channel_block(1001, EXPECTED_JSON) == EXPECTED_JSON


"""
Copy
"""


@responses.activate
def test_create_copy(carousel):
    """
    Ensures that create_copy returns JSON when used with required params
    """
    responses.add(
        "POST", carousel_url("/v1/Copy/bulletins"), json=EXPECTED_JSON, status=200
    )
    assert carousel.create_copy("bulletins", EXPECTED_JSON) == EXPECTED_JSON


"""
Media
"""


@responses.activate
def test_get_media(carousel):
    """
    Ensures that get_media returns JSON when used with required params
    """
    responses.add("GET", carousel_url("/v1/Media"), json=EXPECTED_JSON, status=200)
    assert carousel.get_media(1001) == EXPECTED_JSON


@responses.activate
def test_delete_media(carousel):
    """
    Ensures that delete_media returns a success message str when completed
    successfully
    """
    responses.add("DELETE", carousel_url("/v1/Media"), status=204)
    assert carousel.delete_media(EXPECTED_JSON) == "Media file(s) successfully deleted."


"""
Players
"""


@responses.activate
def test_get_players(carousel):
    """
    Ensures that get_players returns JSON when used without optional params
    """
    responses.add("GET", carousel_url("/v1/Players"), json=EXPECTED_JSON, status=200)
    assert carousel.get_players() == EXPECTED_JSON


@responses.activate
def test_get_players_optional_params(carousel):
    """
    Ensures that get_players returns JSON when used with all optional params
    """
    responses.add("GET", carousel_url("/v1/Players"), json=EXPECTED_JSON, status=200)
    assert carousel.get_players("-L-", "-name", 100, 1) == EXPECTED_JSON


@responses.activate
def test_get_player(carousel):
    """
    Ensures that get_player returns JSON when used with required params
    """
    responses.add(
        "GET", carousel_url("/v1/Players/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.get_player(1001) == EXPECTED_JSON


@responses.activate
def test_update_players(carousel):
    """
    Ensures that update_player returns JSON when used with required params
    """
    responses.add(
        "PUT", carousel_url("/v1/Players/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.update_player(1001, EXPECTED_JSON) == EXPECTED_JSON


"""
PowerSchedules
"""


@responses.activate
def test_get_power_schedules(carousel):
    """
    Ensures that get_power_schedules returns list when used
    """
    responses.add(
        "GET", carousel_url("/v1/PowerSchedules"), json=EXPECTED_JSON, status=200
    )
    assert carousel.get_power_schedules() == EXPECTED_JSON


@responses.activate
def test_create_power_schedule(carousel):
    """
    Ensures that create_power_schedule returns JSON when used with required
    params
    """
    responses.add(
        "POST", carousel_url("/v1/PowerSchedules"), json=EXPECTED_LIST, status=200
    )
    assert carousel.create_power_schedule(EXPECTED_JSON) == EXPECTED_LIST


"""
Zones
"""


@responses.activate
def test_get_zones(carousel):
    """
    Ensures that get_zones returns JSON when used
    """
    responses.add("GET", carousel_url("/v1/Zones"), json=EXPECTED_JSON, status=200)
    assert carousel.get_zones() == EXPECTED_JSON


@responses.activate
def test_get_zone(carousel):
    """
    Ensures that get_zone returns JSON when used with required params
    """
    responses.add("GET", carousel_url("/v1/Zones/1001"), json=EXPECTED_JSON, status=200)
    assert carousel.get_zone(1001) == EXPECTED_JSON


@responses.activate
def test_create_zone(carousel):
    """
    Ensures that create_zone returns JSON when used with required params
    """
    responses.add("POST", carousel_url("/v1/Zones"), json=EXPECTED_JSON, status=200)
    assert carousel.create_zone(EXPECTED_JSON) == EXPECTED_JSON


@responses.activate
def test_update_zone(carousel):
    """
    Ensures that update_zone returns JSON when used with required params
    """
    responses.add("PUT", carousel_url("/v1/Zones/1001"), json=EXPECTED_JSON, status=200)
    assert carousel.update_zone(1001, EXPECTED_JSON) == EXPECTED_JSON


"""
ZoneTags
"""


@responses.activate
def test_get_zone_tags(carousel):
    """
    Ensures that get_zone_tags returns JSON when used with required params
    """
    responses.add("GET", carousel_url("/v1/ZoneTags"), json=EXPECTED_JSON, status=200)
    assert carousel.get_zone_tags() == EXPECTED_JSON


@responses.activate
def test_get_zone_tag(carousel):
    """
    Ensures that get_zone_tag returns JSON when used with required params
    """
    responses.add(
        "GET", carousel_url("/v1/ZoneTags/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.get_zone_tag(1001) == EXPECTED_JSON


@responses.activate
def test_create_zone_tag(carousel):
    """
    Ensures that create_zone_tag returns JSON when used with required params
    """
    responses.add("POST", carousel_url("/v1/ZoneTags"), json=EXPECTED_JSON, status=200)
    assert carousel.create_zone_tag(EXPECTED_JSON) == EXPECTED_JSON


@responses.activate
def test_update_zone_tag(carousel):
    """
    Ensures that update_zone_tag returns JSON when used with required params
    """
    responses.add(
        "PUT", carousel_url("/v1/ZoneTags/1001"), json=EXPECTED_JSON, status=200
    )
    assert carousel.update_zone_tag(1001, EXPECTED_JSON) == EXPECTED_JSON
