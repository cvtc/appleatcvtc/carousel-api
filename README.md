# Carousel API

[![GitLab](https://img.shields.io/gitlab/license/cvtc/appleatcvtc/carousel-api?style=flat-square)]()
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Carousel API is a Carousel Signage API wrapper than 

## Table of Contents

- [Carousel API](#carousel-api)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Install](#install)
  - [Usage](#usage)
  - [Other Notes](#other-notes)
  - [Contributing](#contributing)
  - [License](#license)

## Background

This is a wrapper for the Carousel Signage API with the goal of removing code duplication from Canvas API projects. This wrapper will only gain functionality as I need it for projects.

## Install

To install Carousel API run the following:

```
pip install carousel-api --index-url https://gitlab.com/api/v4/projects/43456134/packages/pypi/simple
```

## Usage

The Carousel API only supports basic auth so we just make a session defined as a variable and then use that for the API access that we need.

```
from os import environ
from carousel_api.carousel import Carousel

CAROUSEL_URL = environ["CAROUSEL_URL"]
CAROUSEL_USERNAME = environ["CAROUSEL_USERNAME"]
CAROUSEL_PASSWORD = environ["CAROUSEL_PASSWORD"]

carousel = Carousel(CAROUSEL_URL, CAROUSEL_USERNAME, CAROUSEL_PASSWORD)

bulletin = carousel.get_bulletin("asd123asd123")
```

## Other Notes

- This API wrapper will most likely never be "finished" it's only purpose is to have a central place to make any future endpoints that I need for Carousel Signage so I'm not duplicating code in projects

## Contributing

This repository contains a Pipfile for easy management of virtual environments
with Pipenv. Run it, but don't create a lockfile, to install the development
dependencies:

```
pipenv install --skip-lock --dev
```

To run the tests and get coverage information, run:

```
pipenv run pytest --cov=src --cov-report=xml --cov-report=term-missing
```

Files are formatted with Black prior to committing. Black is installed in your Pipenv virtual environment. Run it like this before you commit:

```
pipenv run black .
```

## License

[MIT © Bryan Weber.](./LICENSE)