import json
import subprocess
from typing import List
from mimetypes import guess_type
from os.path import basename
from typing import Union

from carousel_api.request_builder import RequestBuilder


def remove_empty_params(params: dict) -> dict:
    """
    Removes all key, value pairs where the value is None

    :param params: Dictionary of params
    """
    params = {k: v for k, v in params.items() if v is not None}

    return params


class Carousel(RequestBuilder):
    def __init__(self, base_url, username, password):
        super().__init__(base_url, username, password)  # pragma: no cover

    """
    Bulletins
    """

    def get_bulletins(self, zoneID: Union[int, str], lastupdate: str = None) -> dict:
        """
        Returns all bulletins for a specified zone by zone ID

        :param zoneID: Zone ID
        :param lastupdate:
            Filter by the last update to the bulletin

            Example: "2023-08-08T09:24:29.98"

        :returns: All zone bulletins information in JSON
        """
        params = remove_empty_params({"zoneID": zoneID, "lastupdate": lastupdate})
        endpoint = "/v1/Bulletins"

        return self._get(endpoint, params)

    def get_bulletin(self, id: Union[int, str]) -> dict:
        """
        Returns a specified bulletin by ID

        :param id: Bulletin ID

        :returns: Specified bulletin information
        """
        endpoint = f"/v1/Bulletins/{id}"

        return self._get(endpoint)

    def update_bulletin(self, id: Union[str, int], data: dict) -> dict:
        """
        Updates a specified bulletin by ID with JSON

        :param id: Bulletin ID
        :param data: JSON data to update bulletin with

        :returns: Updated bulletin information
        """
        endpoint = f"/v1/Bulletins/{id}"

        return self._put(endpoint, data)

    def delete_bulletins(self, ids: List[str], delete_linked: bool = False) -> dict:
        """
        Deletes specified bulletin(s) with the option to delete linked
        bulletins as well

        :param ids: List of bulletin IDs to delete
        :param delete_linked: Whether or not to delete linked bulletins

        :returns: Removed bulletin info
        """
        data = {"IDs": ids, "ApplyToLinked": delete_linked}
        endpoint = "/v1/Bulletins"

        return self._delete(
            endpoint,
            data,
            success_message="Bulletins successfully queued for deletion.",
        )

    """
    Channels
    """

    def get_channels(self) -> list:
        """
        Returns all channel information in JSON

        :returns: All channel information in JSON
        """
        endpoint = "/v1/Channels"

        return self._get(endpoint)

    def get_channel(self, id: Union[int, str]) -> dict:
        """
        Returns specified channel information by ID

        :param id: Channel ID

        :returns: Channel information in JSON
        """
        endpoint = f"/v1/Channels/{id}"

        return self._get(endpoint)

    def create_channel(self, data: dict) -> dict:
        """
        Creates a new channel with JSON data

        :param data: JSON data to create channel with

        :returns: New channel information in JSON
        """
        endpoint = "/v1/Channels"

        return self._post(endpoint, data)

    def update_channel(self, id: Union[int, str], data: dict) -> dict:
        """
        Updates an existing channel with JSON by ID with JSON

        :param id: Channel ID
        :param data: JSON data to update channel with

        :returns: Updated channel information in JSON
        """
        endpoint = f"/v1/Channels/{id}"

        return self._put(endpoint, data)

    """
    ChannelBlocks
    """

    def get_channel_blocks(self) -> list:
        """
        Returns all channel blocks

        :returns: All channel blocks in JSON
        """
        endpoint = "/v1/ChannelBlocks"

        return self._get(endpoint)

    def get_channel_block(self, id: Union[int, str]) -> dict:
        """
        Returns specified channel block by ID

        :param id: Channel block

        :returns: Channel block info in JSON
        """
        endpoint = f"/v1/ChannelBlocks/{id}"

        return self._get(endpoint)

    def create_channel_block(self, data: dict) -> dict:
        """
        Creates a new channel block with JSON data

        :param data: JSON data to create channel block with

        :returns: New channel block info in JSON
        """
        endpoint = "/v1/ChannelBlocks"

        return self._post(endpoint, data)

    def update_channel_block(self, id: Union[int, str], data: dict) -> dict:
        """
        Updates a channel block with JSON by ID

        :param id: Channel block ID
        :param data: JSON data to update channel block with

        :returns: Updated channel block info in JSON
        """
        endpoint = f"/v1/ChannelBlocks/{id}"

        return self._put(endpoint, data)

    """
    Copy
    """

    def create_copy(self, type: str, data: dict) -> dict:
        """
        Creates copy of the requested type defined by the given data

        :param type: bulletins, templates, media, or groups
        :param data:
            JSON data defining the content to copy

            Example:
            {
                "SourceIDs":[
                    "7de041c-3aed-4e10-9690-298ad5b"
                ],
                "ZoneIDs":[
                    "1001",
                    "1002",
                    "1003",
                ],
                "DeleteSources":False,
                "CopyAsLinked":True
            }

        :returns: Async command ID
        """
        endpoint = f"/v1/Copy/{type}"

        return self._post(endpoint, data)

    """
    Media
    """

    def get_media(self, zoneID: int) -> dict:
        """
        Returns all media from a specified zone by ID

        :param zoneID: Zone ID

        :returns: All media information for a specific zone in JSON
        """
        params = {"zoneID": zoneID}
        endpoint = "/v1/Media"

        return self._get(endpoint, params)

    def create_media(
        self, file_path: str, uploadParams: dict
    ) -> dict:  # pragma: no cover
        """
        Uploads a file to the specified zone

        :params file_path: Path to file to upload
        :params uploadParams:
            JSON defining upload params

            Example:
                {
                    "ZoneID": 1,
                    "Type": "Picture",
                    "UserID": "11111111-1111-111-111111111111"
                }

        :returns: New media file information in JSON
        """
        # This uses curl because I couldn't get it working with requests
        # The multipart/form-data gave a boundary error I was not able to fix
        file_name = basename(file_path)
        file_type = guess_type(file_name)[0]
        output = subprocess.check_output(
            [
                "curl",
                "-s",
                "-X",
                "POST",
                f"{self.base_url}/v1/Media",
                "-u",
                f"{self.session.auth[0]}:{self.session.auth[1]}",
                "-H",
                "accept: */*",
                "-H",
                "Content-Type: multipart/form-data",
                "-F",
                f"uploadedFile=@{file_path};type={file_type}",
                "-F",
                f"uploadParams={uploadParams}",
                "-F",
                f"Content-Type={file_type}",
            ]
        )
        return json.loads(output.decode("utf-8"))

    def delete_media(self, data: dict) -> dict:
        """
        Deletes multiple media files with JSON

        :param zoneID: Zone ID
        :param data:
            JSON data to delete multiple

            Example:
            {
                "IDs": [
                    "string"
                ],
                "ApplyToLinked": true
            }

        :returns: Success message stating the media files were deleted
        """
        endpoint = "/v1/Media"
        success_message = "Media file(s) successfully deleted."

        return self._delete(endpoint, data, success_message=success_message)

    """
    Players
    """

    def get_players(
        self,
        search: str = None,
        sort: str = None,
        page_size: int = None,
        page: int = None,
    ) -> list:
        """
        Returns all Carousel players currently registered on the Carousel
        Server.

        :param search:
            Optional: Filters used against player names and player tags in
            order to limit the result set. Terms are broken apart except when
            in "quotes". Additional terms will be ANDed on to the filter to
            further limit the result set. For each of the terms, returns
            results where the term is 1) contained in the player's name and 2)
            has a tag with the exact term (case insensitive).

        :param sort:
            Optional: Result set sort criteria. Prepend "-" for sort
            descending. Sort value can be one of the following:
            "lastcheckinutc", "hostaddress", "channelname", "version", "type",
            "name". Status ascending sort will rank players with a last
            checkin < 2 minutes first, then players with a last checkin < 15
            minutes, and finally all remaining players. A secondary sort by
            name will always be applied to the result set (unless the primary
            sort was already by name). This ensures the final values are always
            sorted by name. Default sort is by name ascending.

        :param page_size:
            Optional: Number of players to return per page. Used to paginate
            the result set to reduce the returned payload's size. Defaults to
            ALL. Results are sorted first, then paginated.

        :param page:
            Optional: specify the 1-based page number. Defaults to 1.

        :returns: Sorted and paged Carousel players in JSON
        """
        params = {
            "search": search,
            "sort": sort,
            "page_size": page_size,
            "page": page,
        }
        endpoint = "/v1/Players"

        return self._get(endpoint, params)

    def get_player(
        self,
        id: Union[int, str],
    ) -> dict:
        """
        Returns player information by ID

        :param id: Player ID

        :returns: Player information in JSON
        """
        endpoint = f"/v1/Players/{id}"

        return self._get(endpoint)

    def update_player(
        self,
        id: Union[int, str],
        data: dict,
    ) -> dict:
        """
        Updates a specific player's information by ID with JSON

        :param id:
            The player Id, which can be retrieved through Carousel.get_players
            endpoint.
        :param data:
            JSON data to update the player with. For syntax information view
            Carousel's API reference at
            https://support.demo.carouselsignage.net/CarouselAPI/swagger/index.html

        :returns: Updated player information in JSON
        """
        endpoint = f"/v1/Players/{id}"

        return self._put(endpoint, data)

    """
    PowerSchedules
    """

    def get_power_schedules(self) -> list:
        """
        Returns all power schedules

        :returns: All power schedules information in a list of dicts
        """
        endpoint = "/v1/PowerSchedules"

        return self._get(endpoint)

    def create_power_schedule(self, data: dict) -> dict:
        """
        Creates a new power schedule with JSON

        :param data:
            JSON data to create new power schedule with

            Example:
            {
                "id": 0,
                "Name": "string",
                "Entries": [
                    {
                    "OnTime": "string",
                    "Duration": 0,
                    "Weekday": 0
                    }
                ]
            }

        :returns: New power schedule information in JSON
        """
        endpoint = "/v1/PowerSchedules"

        return self._post(endpoint, data)

    """
    Zones
    """

    def get_zones(self) -> list:
        """
        Returns all zones

        :returns: All zones in JSON
        """
        endpoint = "/v1/Zones"

        return self._get(endpoint)

    def get_zone(self, id: int) -> dict:
        """
        Returns zone information by ID

        :param id: Zone ID

        :returns: Zone information in JSON
        """
        endpoint = f"/v1/Zones/{id}"

        return self._get(endpoint)

    def create_zone(self, data: dict) -> dict:
        """
        Creates a new zone with JSON

        :param data: JSON data to create zone with

        :returns: Created zone details in JSON
        """
        endpoint = "/v1/Zones"

        return self._post(endpoint, data)

    def update_zone(self, id: Union[str, int], data: dict) -> dict:
        """
        Updates an existing zone with JSON by ID

        :param id: Zone ID
        :param data: JSON data to update zone with

        :returns: Updated zone information in JSON
        """
        endpoint = f"/v1/Zones/{id}"

        return self._put(endpoint, data)

    """
    ZoneTags
    """

    def get_zone_tags(self) -> list:
        """
        Returns all zone tags in JSON

        :returns: All zone tags in JSON
        """
        endpoint = "/v1/ZoneTags"

        return self._get(endpoint)

    def get_zone_tag(self, id: Union[int, str]) -> dict:
        """
        Returns specified zone tag by ID

        :param id: Zone tag ID

        :returns: Zone tag information in JSON
        """
        endpoint = f"/v1/ZoneTags/{id}"

        return self._get(endpoint)

    def create_zone_tag(self, data: dict) -> dict:
        """
        Creates new zone tag with JSON

        :param data: JSON data to create zone tag with

        :returns: New zone tag information in JSON
        """
        endpoint = "/v1/ZoneTags"

        return self._post(endpoint, data)

    def update_zone_tag(self, id: Union[int, str], data: dict) -> dict:
        """
        Updates new zone tag with by ID with JSON

        :param id: Zone tag ID
        :param data: JSON data to update zone tag with

        :returns: Updated zone tag information in JSON
        """
        endpoint = f"/v1/ZoneTags/{id}"

        return self._put(endpoint, data)
