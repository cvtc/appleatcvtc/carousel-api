import requests


class RequestBuilder:
    """
    Handles auth and requests for the rest for the wrapper

    :param base_url:
        Base URL of the Carousel Signage instance
        e.g. "https://example.carouselsignage.net"
    :param username:
        Username for Carousel Signage
    :param password:
        Password for Carousel Signage
    """

    def __init__(self, base_url: str, username: str, password: str):  # pragma: no cover
        self.base_url = base_url + "/CarouselAPI"
        self.session = requests.Session()
        self.session.auth = (username, password)

    def _get(self, endpoint: str, params: dict = None) -> dict:
        """
        Sends get requests given an endpoint and params

        :param endpoint:
            The url section of the API endpoint following the base_url
            e.g. /v1/Bulletins
        :param params: Optional params for the request

        :returns: JSON response of the get request
        """
        full_url = self.base_url + endpoint
        headers = {"Accept": "application/json"}
        response = self.session.get(full_url, headers=headers, params=params)
        response.raise_for_status()

        return response.json()

    def _post(
        self,
        endpoint: str,
        data: dict,
        params: dict = None,
        headers: dict = None,
        files=None,
    ) -> dict:
        """
        Sends post requests given an endpoint and params

        :param endpoint:
            The url section of the API endpoint following the base_url
            e.g. /v1/Bulletins
        :param data: JSON dict used in the put request
        :param params: Optional params for the request
        :param files: File to upload

        :returns: JSON response of the post request
        """
        full_url = self.base_url + endpoint
        if not headers:
            headers = {"Content-type": "application/json"}
        if files:
            response = self.session.post(
                full_url, json=data, params=params, files=files
            )
        else:
            response = self.session.post(
                full_url, json=data, headers=headers, params=params
            )
        response.raise_for_status()

        return response.json()

    def _put(self, endpoint: str, data: dict, params: dict = None) -> dict:
        """
        Sends put requests given an endpoint and params

        :param endpoint:
            The url section of the API endpoint following the base_url
            e.g. /v1/Bulletins
        :param data: JSON dict used in the put request
        :param params: Optional params for the request

        :returns: JSON response of the put request
        """
        full_url = self.base_url + endpoint
        headers = {"Content-type": "application/json"}
        response = self.session.put(full_url, json=data, headers=headers, params=params)
        response.raise_for_status()

        return response.json()

    def _delete(
        self,
        endpoint: str,
        data: dict = None,
        params: dict = None,
        success_message: str = None,
    ) -> dict:
        """
        Sends delete requests given an endpoint and params

        :param endpoint:
            The url section of the API endpoint following the base_url
            e.g. /v1/Bulletins
        :param data: JSON dict used in the delete request
        :param params: Optional params for the request
        :param success_message:
            Success Message str saying that the Media files were deleted

        :returns: JSON response of the get request
        """
        full_url = self.base_url + endpoint
        headers = {"Accept": "application/json"}
        response = self.session.delete(
            full_url, headers=headers, json=data, params=params
        )
        response.raise_for_status()

        if success_message:
            return success_message
        else:
            return response.json()
